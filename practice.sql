USE music_db;

DROP TABLE reviews;

CREATE TABLE reviews(
	id INT NOT NULL AUTO_INCREMENT,
	review VARCHAR(500) NOT NULL,
	datetime_created DATETIME NOT NULL,
	rating INT NOT NULL,
	PRIMARY KEY (id)
);

INSERT INTO reviews(review, rating, datetime_created) VALUES 
("Twice is mid, blackpink better", 2, "2023-05-03"),
("My favorite is HYLT!", 5, "2023-05-03"),
("I LOVE BTS!", 5, "2023-05-03"),
("Rythm is repetitive", 1, "2023-05-03"),
("SVT Fighting!", 5, "2023-05-03");

SELECT * FROM reviews;
SELECT * FROM reviews WHERE rating = 5;
SELECT * FROM reviews WHERE rating = 1;
UPDATE reviews SET rating = 5;



CREATE DATABASE course_db;

DROP DATABASE course_db;

CREATE DATABASE course_db;

USE course_db;

CREATE TABLE students (
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE subjects (
	id INT NOT NULL AUTO_INCREMENT,
	course_name VARCHAR(100) NOT NULL,
	schedule VARCHAR(100) NOT NULL,
	instructor VARCHAR(100) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE enrollments (
	id INT NOT NULL AUTO_INCREMENT,
	student_id INT NOT NULL,
	subject_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_enrollments_student_id
		FOREIGN KEY (student_id) REFERENCES students(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_enrollments_subject_id
		FOREIGN KEY (subject_id) REFERENCES subjects(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

INSERT INTO students (username, password, full_name, email) VALUES ("nathanieljaen", "natnat", "nathaniel jaen", "nath@gmail.com");

INSERT INTO subjects (course_name, schedule, instructor) VALUES ("Java OOP", "5pm-10pm", "Ms. Camille");

INSERT INTO enrollments (student_id, subject_id) VALUES (1, 1);


